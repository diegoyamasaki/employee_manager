# Employee Manager and Employee API

## Subir o ambiente:

###API:
Pode subir um ambiente rapidamente usando docker para isso basta ter instalado o docker e executar o comando
```
docker-compose up
``` 
Irá criar todo a ambiente necessario para rodar a api.
Para executar localmente é preciso instalar as bibliotecas no seu ambiente ou no da aplicação utilizando o virtualenv
```
pip install -r requirements.txt
```

Para executar o teste pode utilizar o arquivo Makefile e executar o comando abaixo somente:
```
make run
```

###Testes:
Para testar é pode usar o proprio ambiente ou criar um ambiente excluiso para a aplicação utilizando o virtualenv
É necessário executar o comando abaixo para instalar as bibliotecas utilizadas no teste.
```
pip install -r requirements-dev.txt
```

Para executar o teste pode utilizar o arquivo Makefile e executar o comando abaixo somente:
```
make test
```

Api de criação e gerenciamento de Empregados.

Para usar o admin acesse pela url local:

http://localhost:8000/admin

Pode acessar pelos usuários abaixo:

###superuser
username: diegoyamasaki
password: 040286

###employee admin staff
username: admin
password: teste@2019

##API:
Para retornar todos os empregados acesse a url abaixo:

###GET - http://localhost:8000/employee/

Ex de retorno:
```
[
    {
        "id": 2,
        "name": "Diego Yamasaki",
        "email": "yamasaki.diego@gmail.com",
        "department": "TI"
    },
    {
        "id": 5,
        "name": "Marcos santos silverio",
        "email": "marcos.silverio@testeluiza.com",
        "department": "Financeiro"
    },
    {
        "id": 6,
        "name": "Pedro Rocha",
        "email": "pedrorocha2019@gmail.com",
        "department": "TI"
    }
]
```
Para adicionar um novo empregado use a url abaixo e siga o payload:

###POST - http://localhost:8000/employee/

Payload:
```
{
    "name": "Pedro Rocha",
    "email": "pedrorocha2019@gmail.com",
    "department": "TI"
}
```
Todos os campos são obrigatorios, o campo email é necessário enviar um email valido.

Para alterar a informação de um empregao  use a url abaixo e o payload:

###PUT - http://localhost:8000/employee/<id_employee>

id_employee é o id do cliente que é retornado na api de busca dos clientes

Payload:
```
{
    "name": "Pedro Rocha",
    "email": "pedrorocha2019@gmail.com",
    "department": "TI"
}
```
Todos os campos são obrigatorios, o campo email é necessário enviar um email valido.

Para buscar um empregado use a url abaixo:

###GET -  http://localhost:8000/employee/<id_employee>

id_employee é o id do cliente que é retornado na api de busca dos clientes

Para deletar um empregado use a url abaixo:

###DELETE - http://localhost:8000/employee/<id_employee>

id_employee é o id do cliente que é retornado na api de busca dos clientes