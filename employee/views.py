from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from employee_manager.models import Employee as EmployeeModel
from .serializers import EmployeeSerializers
from rest_framework.parsers import JSONParser

class Employee(APIView):

    def get(self, request):
        employee = EmployeeModel.objects.all()
        serializer = EmployeeSerializers(employee, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        data = JSONParser().parse(request)
        serializer = EmployeeSerializers(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


class EmployeeDetail(APIView):

    def get(self, request, id):
        try:
            employee = EmployeeModel.objects.get(pk=id)
        except EmployeeModel.DoesNotExist:
            return HttpResponse(status=404)
        serializer = EmployeeSerializers(employee)
        return Response(serializer.data)
    
    def put(self, request, id):
        try:
            employee = EmployeeModel.objects.get(pk=id)
        except EmployeeModel.DoesNotExist:
            return HttpResponse(status=404)
        data = JSONParser().parse(request)
        serializer = EmployeeSerializers(employee, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)
    
    def delete(self, request, id):
        try:
            employee = EmployeeModel.objects.get(pk=id)
        except EmployeeModel.DoesNotExist:
            return HttpResponse(status=404)
        employee.delete()
        return HttpResponse(status=200)
