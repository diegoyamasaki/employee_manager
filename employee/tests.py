import json
from django.test import TestCase
from rest_framework.test import RequestsClient
from employee_manager.models import Employee
from .serializers import EmployeeSerializers
class EmployeeTestCase(TestCase):

    def setUp(self):
        Employee.objects.create(name='Teste', email='teste@teste.com', department='Testing')
        Employee.objects.create(name='Teste2', email='teste2@teste.com', department='Testing')
        self.request = RequestsClient()
    
    def test_get_all_employee(self):
        response = self.request.get('http://testserver/employee/')
        qtd_employee = len(response.json())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(qtd_employee, 2)
    
    def test_post_employee(self):
        data = {"name": "Pedro Rocha","email": "pedrorocha2019@gmail.com","department": "TI"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 201)
        employee = response.json()
        self.assertIsNotNone(employee)
    
    def test_put_employee(self):
        data = {"name": "Teste updated","email": "teste@teste.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 200)
        employee = response.json()
        self.assertIsNotNone(employee)
    
    def test_get_employee(self):
        response = self.request.get('http://testserver/employee/1')
        self.assertEqual(response.status_code, 200)
        employee = response.json()
        self.assertIsNotNone(employee)
    
    def test_delete_employee(self):
        response = self.request.delete('http://testserver/employee/1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, '')
    
    def test_delete_not_found_employee(self):
        response = self.request.delete('http://testserver/employee/100')
        self.assertEqual(response.status_code, 404)
    
    def test_get_not_found_employee(self):
        response = self.request.get('http://testserver/employee/100')
        self.assertEqual(response.status_code, 404)
    
    def test_put_not_found_employee(self):
        data = {"name": "Teste updated","email": "teste@teste.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/100',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 404)
    
    def test_put_email_required_employee(self):
        data = {"name": "Teste updated","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['This field is required.'])
    
    def test_put_name_required_employee(self):
        data = {"email": "teste@teste.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['name'], ['This field is required.'])
    
    def test_put_department_required_employee(self):
        data = {"name": "Teste updated","email": "teste@teste.com"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['department'], ['This field is required.'])
    
    def test_post_name_required_employee(self):
        data = {"email": "pedrorocha2019@gmail.com","department": "TI"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['name'], ['This field is required.'])
    
    def test_post_email_required_employee(self):
        data = {"name": "Teste updated","department": "Testing"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['This field is required.'])
    
    def test_post_department_required_employee(self):
        data = {"name": "Teste updated","email": "teste@teste.com"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['department'], ['This field is required.'])
    
    def test_valid_employee_serializer(self):
        data = {"name": "Teste updated","email": "teste1234@teste.com","department": "Testing"}
        employee = EmployeeSerializers(data=data)
        self.assertTrue(employee.is_valid())
    
    def test_is_not_valid_employee_serializer(self):
        data = {"name": "Teste updated","email": "teste@.com","department": "Testing"}
        employee = EmployeeSerializers(data=data)
        self.assertFalse(employee.is_valid())
    
    def test_save_employee_serializer(self):
        data = {"name": "Teste serial","email": "teste123@teste.com","department": "Testing"}
        employee = EmployeeSerializers(data=data)
        self.assertTrue(employee.is_valid())
        employee.save()
        employee_new = Employee.objects.get(name='Teste serial')
        self.assertIsNotNone(employee_new)
    
    def test_update_employee_serializer(self):
        data = {"name": "Teste serial","email": "teste@teste.com","department": "Testing"}
        employee = Employee.objects.get(name='Teste')
        serializer = EmployeeSerializers(employee, data=data)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        employee_updated = Employee.objects.get(name='Teste serial')
        self.assertIsNotNone(employee_updated)

    
    def test_email_not_valid_post_employee(self):
        data = {"name": "Teste updated","email": "teste@.com","department": "Testing"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['Enter a valid email address.'])
    
    def test_email_not_valid_put_employee(self):
        data = {"name": "Teste updated","email": "teste@.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['Enter a valid email address.'])
    
    def test_name_not_valid_post_employee(self):
        data = {"name": '',"email": "teste@teste.com","department": "Testing"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['name'], ['This field may not be blank.'])

    def test_name_not_valid_put_employee(self):
        data = {"name": '',"email": "teste@teste.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['name'], ['This field may not be blank.'])
    

    def test_department_not_valid_post_employee(self):
        data = {"name": 'Teste',"email": "teste@teste.com","department": ""}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['department'], ['This field may not be blank.'])
    
    def test_department_not_valid_put_employee(self):
        data = {"name": 'teste',"email": "teste@teste.com","department": ""}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['department'], ['This field may not be blank.'])
    
    def test_email_already_exist_post_employee(self):
        data = {"name": "Teste","email": "teste@teste.com","department": "Testing"}
        response = self.request.post(
            'http://testserver/employee/',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['employee with this email already exists.'])
    
    def test_email_already_exist_put_employee(self):
        data = {"name": "Teste","email": "teste2@teste.com","department": "Testing"}
        response = self.request.put(
            'http://testserver/employee/1',
            headers={'Content-Type': 'application/json'},
            data=json.dumps(data))
        self.assertEqual(response.status_code, 400)
        data_required = response.json()
        self.assertEqual(data_required['email'], ['employee with this email already exists.'])
