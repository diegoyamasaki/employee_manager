from . import views
from django.conf.urls import include, url

app_name = 'employee'

urlpatterns = [
    # GET /
    url(r'^employee/$', views.Employee.as_view()),
    url(r'^employee/(?P<id>[0-9]+)$', views.EmployeeDetail.as_view())
]