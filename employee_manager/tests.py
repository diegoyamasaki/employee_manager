from django.test import TestCase
from .models import Employee

# Create your tests here.
class EmployeeManagerTestCase(TestCase):

    def setUp(self):
        Employee.objects.create(name='Teste', email='teste@teste.com', department='Testing')
    
    def test_employee_get_object(self):
        employee = Employee.objects.get(name='Teste')
        self.assertIsNotNone(employee)
    
    def test_employee_save(self):
        Employee.objects.create(name='Teste2', email='teste2@teste.com', department='Testing')
        employee = Employee.objects.get(name='Teste2')
        self.assertIsNotNone(employee)
    
    def test_employee_delete(self):
         employee = Employee.objects.get(name='Teste')
         employee.delete()
         with self.assertRaises(Employee.DoesNotExist):
            Employee.objects.get(name='Teste')
    
    def test_employee_update(self):
        employee = Employee.objects.get(name='Teste')
        employee.name = "Teste123"
        employee.save()
        employee2 = Employee.objects.get(name='Teste123')
        self.assertIsNotNone(employee2)