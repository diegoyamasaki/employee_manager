from django.db import models

# Create your models here.
class Employee(models.Model):
    name = models.CharField(
        max_length=255,
        null=False,
        blank=False
    )
    email = models.EmailField(
        max_length=254,
        null=False,
        unique=True
    )
    department = models.CharField(
        max_length=255,
        null=False,
        blank=False
    )
    objects = models.Manager()